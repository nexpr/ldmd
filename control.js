import { Blob } from "buffer"
import fs from "fs"
import jsdom from "jsdom"
import markdownit from "markdown-it"
import path from "path"
import { article_manager, file_manager } from "./ld-api.js"

const md = markdownit({ html: true })
const { JSDOM } = jsdom

const main = () => {
	const [action, target] = process.argv.slice(2)

	if (!action || !target) {
		exit("操作と対象を指定してください")
	}

	if (action === "check") {
		check(target)
		console.log("OK")
	} else if (action === "up") {
		up(target).then(() => {
			console.log("完了しました")
		})
	} else {
		exit("不正な操作内容です\ncheck または up から指定してください")
	}
}

setImmediate(main)

const exit = (msg) => {
	msg && console.error(msg)
	process.exit(msg ? 1 : 0)
}

const check = (target) => {
	if (!target.endsWith(".md")) {
		exit("markdown ファイルを指定してください")
	}

	const text = fs.readFileSync(target).toString()
	const html = md.render(text)
	const dom = new JSDOM(`<body>${html}</body>`)
	const body = dom.window.document.body
	const comment = body.firstChild

	if (comment.nodeName !== "#comment") {
		exit("コメントノードから始まっていません")
	}

	const meta = JSON.parse(comment.nodeValue)

	if (!meta.title) {
		exit("タイトルを入力してください")
	}

	const img_paths = [...body.querySelectorAll("img")]
		.map(img => path.join(path.dirname(target), img.src))

	for (const img_path of img_paths) {
		const exists = fs.existsSync(img_path) && fs.statSync(img_path).isFile()
		if (!exists) {
			exit(`画像ファイル ${img_path} が見つかりません`)
		}
	}

	return { meta, img_paths }
}

const up = async (target) => {
	const { meta, img_paths } = check(target)

	const { lists: root_lists } = await file_manager.list()
	const article_dir_info = root_lists.find(item => item.name === "article" && item.is_dir)

	// 必須フォルダがないのはエラーにする
	// アカウントミスを防ぐのも兼ねて article フォルダは自動で作らない
	if (!article_dir_info) {
		exit("ルートに article フォルダがみつかりませんでした")
	}

	const md_blob = new Blob([fs.readFileSync(target)])
	await file_manager.upload(article_dir_info.id, path.basename(target), md_blob)

	const md_name = path.basename(target, ".md")
	const img_dir_info = await (async () => {
		{
			const { lists: article_lists } = await file_manager.list(article_dir_info.id)
			const img_dir_info = article_lists.find(item => item.name === md_name && item.is_dir)
			if (img_dir_info) return img_dir_info
		}

		// ない場合は作る
		await file_manager.mkdir(md_name, article_dir_info.id)

		{
			// レスポンスで id を受け取れないので再度一覧取得が必要
			const { lists: article_lists } = await file_manager.list(article_dir_info.id)
			return article_lists.find(item => item.name === md_name && item.is_dir)
		}
	})()

	for (const img_path of img_paths) {
		const img_blob = new Blob([fs.readFileSync(img_path)])
		await file_manager.upload(img_dir_info.id, path.basename(img_path), img_blob)
	}

	if (meta.article_id) {
		await article_manager.put(meta.article_id, meta.title, getCommonArticleBody(md_name))
	} else {
		const res = await article_manager.post(meta.title, getCommonArticleBody(md_name))
		console.log("created\n" + res)
	}
}

const getCommonArticleBody = (md_name) => {
	// ファイル名に 「"」 は来ないのでエスケープは省略
	return `<script src="/render.js?md=${md_name}"></script>`
}
