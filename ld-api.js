import config from "./config.js"

const file_manager_base_api_url = `https://livedoor.blogcms.jp/blog/${config.blog_name}/file_manager/`
const aritcle_base_api_url = `https://livedoor.blogcms.jp/atompub/${config.blog_name}/article`

export const file_manager = {
	request: (api_type, formdata) => {
		return fetch(file_manager_base_api_url + api_type, {
			method: "POST",
			headers: {
				"X-LDBlog-Token": config.file_manager_password,
			},
			body: formdata,
		}).then(res => res.json())
	},
	list: async (dir_id) => {
		const formdata = new FormData()

		if (dir_id) {
			formdata.append("dir_id", dir_id)
		}

		return file_manager.request("list", formdata)
	},
	mkdir: async (name, dir_id) => {
		const formdata = new FormData()

		formdata.append("name", name)

		if (dir_id) {
			formdata.append("dir_id", dir_id)
		}

		return file_manager.request("create_dir", formdata)
	},
	upload: async (dir_id, name, upload_data) => {
		const formdata = new FormData()

		formdata.append("dir_id", dir_id)
		formdata.append("name", name)
		formdata.append("upload_data", upload_data)

		return file_manager.request("upload", formdata)
	},
}

const hEscape = (str) =>
	str.replace(/[<>&"]/g, c => {
		if (c === "<") return "&lt;"
		if (c === ">") return "&gt;"
		if (c === "&") return "&amp;"
		if (c === '"') return "&quot;"
	})

export const article_manager = {
	request: (method, article_id, body) => {
		const base64 = Buffer.from(config.livedoor_id + ":" + config.atom_pub_password).toString("base64")
		const url = article_id ? (aritcle_base_api_url + "/" + article_id) : aritcle_base_api_url
		return fetch(url, {
			method,
			headers: {
				Authorization: `Basic ${base64}`,
			},
			body,
		}).then(res => res.text())
	},
	makeBody: ({ title, body, category, draft }) => {
		// dprint-ignore
		return `
			<?xml version="1.0"?>
			<entry xmlns="http://www.w3.org/2005/Atom" xmlns:app="http://www.w3.org/2007/app">
				<title>${hEscape(title)}</title>
				<content type="text/html" xml:lang="ja">${hEscape(body)}</content>
				${
					category
						? `<category scheme="https://livedoor.blogcms.jp/atompub/${config.blog_name}/category" term="${hEscape(category)}"/>`
						: ""
				}
				<app:control>
					<app:draft>${draft ? "yes" : "no" }</app:draft>
				</app:control>
			</entry>
		`.trim()
	},
	get: async () => {
		return article_manager.request("GET")
	},
	post: async (title, body, { category, draft } = {}) => {
		return article_manager.request("POST", null, article_manager.makeBody({ title, body, category, draft }))
	},
	put: async (article_id, title, body, { category, draft } = {}) => {
		return article_manager.request("PUT", article_id, article_manager.makeBody({ title, body, category, draft }))
	},
}
