const article_body_elem = document.querySelector(".article-body")
const md_name = new URL(document.currentScript.src, location).searchParams.get("md")
const load = () =>
	Promise.all([
		new Promise((resolve, reject) => {
			const script = document.createElement("script")
			script.src = "https://cdnjs.cloudflare.com/ajax/libs/markdown-it/13.0.1/markdown-it.min.js"
			script.onload = resolve
			script.onerror = reject
			document.head.append(script)
		}),
		fetch(`/article/${md_name}.md`).then(res => res.text()),
	])
const render = ([, text]) => {
	const tpl = document.createElement("template")
	tpl.innerHTML = markdownit({ html: true }).render(text)
	for (const img of tpl.content.querySelectorAll("img")) {
		img.src = "/article/" + img.getAttribute("src")
	}
	article_body_elem.replaceChildren(tpl.content)
}
load().then(render).catch(err => {
	console.error(err)
	article_body_elem.innerHTML = `読み込みに失敗しました`
})
